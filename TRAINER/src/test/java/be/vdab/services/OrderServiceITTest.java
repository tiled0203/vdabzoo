package be.vdab.services;

import be.vdab.domain.Order;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OrderServiceITTest {
    private OrderService orderService = new OrderService();

    @Test
    public void addOrder() {
        Order order = new Order("test", 50.0);
        orderService.addOrder(order);

        assertEquals(1,orderService.getOrder().size());
    }

    @Test
    public void assertPrice(){
        Order order = new Order("test", 50.0);
        orderService.addOrder(order);
        assertEquals(32.5, order.getPrice(), 0);
    }

    @Test
    public void name() {
        assertEquals("Name: test Price: 50.0", new Order("test" , 50.0).toString());
    }
}
