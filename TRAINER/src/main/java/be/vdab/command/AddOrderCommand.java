package be.vdab.command;

import be.vdab.domain.Order;
import be.vdab.services.OrderService;

import java.util.Scanner;

public class AddOrderCommand implements Command{
    private Scanner scanner = new Scanner(System.in);
    private OrderService orderService = new OrderService();


    @Override
    public void execute() {
        System.out.println("give in price");
        double price = scanner.nextDouble();
        System.out.println("Give in name");
        String name = scanner.next();

        orderService.addOrder(new Order(name, price));
        orderService.getOrder().stream().forEach(order -> {
            System.out.printf("order: %s , price: %f\n" , order.getName(), order.getPrice());
        });
    }
}
