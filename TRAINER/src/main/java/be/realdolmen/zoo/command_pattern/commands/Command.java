package be.realdolmen.zoo.command_pattern.commands;

@FunctionalInterface
public interface Command {
    void execute() ;
}
